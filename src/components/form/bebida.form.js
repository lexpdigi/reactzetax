/**
 * Componente Customizado de Formulario
 * 
 */




 import {Button} from '@material-ui/core';
 import { InputCustom } from '../input-custom';
 import {Form} from '@unform/web'
 const FormCustom = ({id,initialData,handleSubmit})=>{
     return(
         <div style={{marginTop:80,marginLeft:20}}>
             <Form id={id}  initialData={initialData} onSubmit={handleSubmit}>
                 <InputCustom required name={'titulo'} label="Titulo" variant="outlined"  type={'text'}/>
                 <InputCustom  label="Descrição"  name={'descricao'} variant="outlined"   type={'text'} />
            
                 <Button  type={'submit'} variant="contained" color="primary">
                     Salvar
                 </Button>
             </Form>
         </div>
     )
 }
 
 export default FormCustom;
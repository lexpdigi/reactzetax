 /**
 *Saga Principal que agrupa todas as sagas
 * 
 */
 import { all, call } from 'redux-saga/effects';
 import { bebidaSagas } from '../saga/bebida';
 
 
 export default function* rootSaga(){
     yield all([
         call(bebidaSagas)
     ])
 }
  /**
 *Reducer Principal que agrupa todos os reducers
 * 
 */
    
 import { combineReducers } from "redux";

 import bebidaReducer from './bebida/index'
 
  const rootReducer = combineReducers({
     bebida:bebidaReducer
  });
 
 export default rootReducer;
import HeaderNav from './pages/header_nav';
import Router from './routes/router.component';

function App() {
  return (
    <div>
      <HeaderNav/>
      <Router/>
    </div>
  );
}

export default App;
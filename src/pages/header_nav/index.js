import React from 'react';
import {
	AppBar,
	makeStyles,
	Toolbar,
	Button,
    ListItemText
} from '@material-ui/core';

import { AccountCircle } from '@material-ui/icons';
import {withRouter} from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
	root: {},
	drawer: {
		width: 240,
		flexShrink: 0,
	},
	drawerPaper: {
		width: 240,
	},
	grow: {
		flexGrow: 1,
	},
	spaceLeft: {
		marginLeft: 20,
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
	},
	logo: {
		height: 35,
	},
	icons: {
		paddingRight: theme.spacing(5),
	},
	menuIcon: {
		paddingRight: theme.spacing(5),
		paddingLeft: theme.spacing(6),
	},
	listItemText: {
		fontSize: 14,
	},
	drawerContainer: {
		backgroundColor: '#888',
	},
}));

const HeaderNav = ({ items, history }) => {
	const classes = useStyles();
	

		return (
			<div className={classes.root}>
				<AppBar color="primary" className={classes.appBar}>
					<Toolbar>

                    <AccountCircle fontSize={'medium'} />
                    <div className={classes.grow} />
                  

                    <Button
							color="inherit"
							onClick={() => {}}
						>
                        <ListItemText>
                            Listagem
                        </ListItemText>
						</Button>
                        <div className={classes.spaceLeft}/>
						<Button
							color="inherit"
							onClick={() => {}}
						>
                        <ListItemText>
                            Cadastro
                        </ListItemText>
						</Button>
					</Toolbar>
				</AppBar>
				</div>
        )
	
};

export default withRouter(HeaderNav);

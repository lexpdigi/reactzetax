  /**
 * Página de Listagem de  Bebidas
 * 
 */
    

   import {withRouter} from 'react-router-dom';
   import { useState,useEffect } from 'react';
    import { api } from '../../requests/api';
    import ListItemX from './listItem'
    import Spinner from '../../components/loading/loading.compont'
    import './list.css';
 
   const ListPage = ({history})=>{


    const [bebidas,setBebidas] = useState([]);
    const [loading,setLoading] = useState(true);

    useEffect( () => {
          fetchData();
    },[]);

    async function fetchData() {
        let resp = await api.get('/beers');
        setBebidas(resp.data);

       setLoading(false);
        
    }

    

    if(loading){
       return <Spinner/>
    }
      
  
           return (
             <div className='bebida_list'>
                 {
                     bebidas.map((val,idx) => (
                         <ListItemX data={val} key={idx}/>
                     ))
                 }
             </div>
               
           )
       
   
      
   }
   
   export default withRouter(ListPage);
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
	root: {
		maxWidth: 345,
		margin: '1.25rem',
	},
	media: {
		height: 140,
	}
});


function ListItemX({ data }) {
	const classes = useStyles();

    const {name, description, image_url} = data;

	return (
		<Card className={classes.root}>
			<CardActionArea>
				<CardContent>
                <span style={{float:'right',fontSize:30,cursor:'pointer'}}> &times;</span>
                <img src={image_url} style={{display:'block',margin:'auto',width:'80px',obectFit:'cover',textAlign:'center'}}/>
					<Typography
						gutterBottom
						variant="h5"
						component="h2"
                        style={{textAlign:'center',marginTop:'0.5rem'}}
					>
						{name}
					</Typography>
					<Typography gutterBottom>
						<span>{description}</span>
						<br />
					</Typography>
				</CardContent>
			</CardActionArea>
		</Card>
	);
}

export default ListItemX;

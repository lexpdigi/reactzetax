 /**
 *Parte que comunica com api
 * 
 */

 import axios from 'axios'

 const request = {
    "URL_DEV":' https://api.punkapi.com/v2/'
}

let api = axios.create({
  baseURL: request.URL_DEV, 
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    }
  ,
  validateStatus: (status) => status < 500
});

export {api};